﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlador.Clases
{
    public class TokenMessage
    {
        public string Message { get; set; }
        public string Token { get; set; }
        public string TipoUsuario { get; set; }
        public string Email { get; set; }
        public int user_id { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }
}
