﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Controlador.Clases
{
    public class Reporte
    {

        public string Producto { get; set; }
        public int Toneladas { get; set; }

        public static List<Reporte> GetReporte(DateTime fechaInicio, DateTime fechaTermino)
        {

            var api = new ServicioWebWCF.ServicioClient();

            var datos = api.GetReporte(fechaInicio, fechaTermino);

            var reporte = JsonConvert.DeserializeObject<List<Reporte>>(datos);

            return reporte;

        }

        

    }
}
