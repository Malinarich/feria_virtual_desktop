﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;


namespace Controlador.Clases
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int TipoUsuario { get; set; }
        private string Token { get; set; }
        public int Telefono { get; set; }

        public TokenMessage IniciarSesion()
        {
            var msEnviar = new MemoryStream();
            
            var ser = new DataContractJsonSerializer(typeof(Usuario));//colocar el objeto
            ser.WriteObject(msEnviar, this);
            byte[] jsonEnviar = msEnviar.ToArray();
            msEnviar.Close();

            var stringUserJson = Encoding.UTF8.GetString(jsonEnviar, 0, jsonEnviar.Length);

            //string stringUserJson = "{ 'password':'admin1','tipouserid':'','username':'admin1'}";

            ServicioWebWCF.ServicioClient Llamados = new ServicioWebWCF.ServicioClient();

            var respuesta = Llamados.IniciarSesion(stringUserJson);

            var msRecibir = new MemoryStream(Encoding.Unicode.GetBytes(respuesta));
            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(TokenMessage));
            TokenMessage TMjson = (TokenMessage)deserializer.ReadObject(msRecibir);

            
            
            return TMjson;
        }

    }
}
