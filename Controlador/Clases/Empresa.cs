﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Controlador.Clases
{
    public class Empresa
    {

        public int Id { get; set; }
        public string NombreEmpresa { get; set; }


        public static List<Empresa> GetEmpresas()
        {
            ServicioWebWCF.ServicioClient cliente = new ServicioWebWCF.ServicioClient();

            // string jsonEmpresas = cliente.GetEmpresas();

            //var msRecibir = new MemoryStream(Encoding.Unicode.GetBytes(jsonEmpresas));
            //DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(List<Empresa>));
            //List<Empresa> userjson = (List<Empresa>)deserializer.ReadObject(msRecibir);

            
            //return userjson;
            return new List<Empresa>();
        }
    }
}
