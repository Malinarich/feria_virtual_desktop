﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Controlador.Clases
{
    public class ReporteTransportistas
    {
        public Transportista Transportista { get; set; }
        public double Puntuacion { get; set; }

        public static List<ReporteTransportistas> GetReporte(int rango)
        {
            var api = new ServicioWebWCF.ServicioClient();

            var datos = api.ReporteTransportistas(rango);

            var reporte = JsonConvert.DeserializeObject<List<ReporteTransportistas>>(datos);

            return reporte;
        }

    }
}
