﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MessageBox
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
        public partial class W_MessageBox : Window
        {

            public W_MessageBox()
            {
                InitializeComponent();
            }

            private void gBar_MouseDown(object sender, MouseButtonEventArgs e)
            {
                try
                {
                    DragMove();
                }
                catch
                {

                }
            }
            private void btnClose_Click(object sender, RoutedEventArgs e)
            {
                Close();
            }

            DoubleAnimation anim;
            private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
            {
                Closing -= Window_Closing;
                e.Cancel = true;
                anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(0.3));
                anim.Completed += (s, _) => this.Close();
                this.BeginAnimation(UIElement.OpacityProperty, anim);
            }
        }
    }
