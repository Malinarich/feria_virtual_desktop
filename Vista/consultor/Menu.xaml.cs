﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vista.paginas;

namespace Vista.consultor
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {

        bool EstadoCerrado = true;

        public string Nombre;
        public string Email;
        public string Rut;


        public Menu()
        {
            InitializeComponent();

            var ini = new Inicio();


            frMenu.Content = ini;
        }

        public void cerrar()
        {
            if (EstadoCerrado)
            {
                Storyboard sb = this.FindResource("OpenMenu") as Storyboard;

                sb.Begin();


            }
            else
            {
                Storyboard sb = this.FindResource("CloseMenu") as Storyboard;

                sb.Begin();
            }

            EstadoCerrado = !EstadoCerrado;
        }

        private void btnCerrarSesion_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Login());
        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            cerrar();
        }

        private void btnPerfil_Click(object sender, RoutedEventArgs e)
        {
            Perfil perf = new Perfil();

            //perf.tbRut.Text = TokenUser;
            perf.txtNombre.Text = Nombre;
            perf.txtEmail.Text = Email;
            perf.tbRut.Text = Rut;
            frMenu.Content = perf;
        }

        private void btnReporteGeneral_Click(object sender, RoutedEventArgs e)
        {

            ReporteGeneral re = new ReporteGeneral();
            frMenu.Content = re;
        }
    }
}
