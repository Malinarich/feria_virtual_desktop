﻿#pragma checksum "..\..\..\paginas\Menu.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "89EDC661EE735403D8C43CA26C80FF8E848C5820F23C4F46BA2672144366092B"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Vista.paginas;


namespace Vista.paginas {
    
    
    /// <summary>
    /// Menu
    /// </summary>
    public partial class Menu : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Frame frMenu;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMenu;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MenuButton;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle rectangle;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle rectangle1;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle rectangle2;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCerrarSesion;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProceso;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRVenta;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTransportistas;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProductores;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\..\paginas\Menu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPerfil;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Vista;component/paginas/menu.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\paginas\Menu.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.frMenu = ((System.Windows.Controls.Frame)(target));
            return;
            case 2:
            this.GridMenu = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.MenuButton = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\paginas\Menu.xaml"
            this.MenuButton.Click += new System.Windows.RoutedEventHandler(this.MenuButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.rectangle = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 5:
            this.rectangle1 = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 6:
            this.rectangle2 = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 7:
            this.btnCerrarSesion = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\..\paginas\Menu.xaml"
            this.btnCerrarSesion.Click += new System.Windows.RoutedEventHandler(this.btnCerrarSesion_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnProceso = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\..\paginas\Menu.xaml"
            this.btnProceso.Click += new System.Windows.RoutedEventHandler(this.btnProceso_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnRVenta = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.btnTransportistas = ((System.Windows.Controls.Button)(target));
            
            #line 134 "..\..\..\paginas\Menu.xaml"
            this.btnTransportistas.Click += new System.Windows.RoutedEventHandler(this.btnTransportistas_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnProductores = ((System.Windows.Controls.Button)(target));
            
            #line 146 "..\..\..\paginas\Menu.xaml"
            this.btnProductores.Click += new System.Windows.RoutedEventHandler(this.btnProductores_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnPerfil = ((System.Windows.Controls.Button)(target));
            
            #line 158 "..\..\..\paginas\Menu.xaml"
            this.btnPerfil.Click += new System.Windows.RoutedEventHandler(this.btnPerfil_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

