﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para rTransporte.xaml
    /// </summary>
    public partial class rTransporte : Page
    {
        public rTransporte()
        {
            InitializeComponent();

            
            var ListP = new List<Persona>();

           
            for (int i = 0; i < 10; i++)
            {
                var p = new Persona();

                p.Nombre = i.ToString();
                p.Apellido = i.ToString();
                p.Edad = i;

                ListP.Add(p);


            }

            lvDataBinding.ItemsSource = ListP;


        }


        public class Persona
        {
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public int Edad { get; set; }
        }
    }
}
