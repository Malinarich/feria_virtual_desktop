﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Controlador.Clases;
using Controlador.ServicioWebWCF;
using MessageBox;

namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para ResgistroTransportista.xaml
    /// </summary>
    public partial class ResgistroTransportista : Page
    {
        public ResgistroTransportista()
        {
            InitializeComponent();
        }
        
        private void btnRegistrar_Click(object sender, RoutedEventArgs e)
        {
            Transportista trans = new Transportista();

            trans.Rut = txtRut.Text;
            trans.Nombre = txtEmpresa.Text;
            trans.Email = txtEmail.Text;
            trans.TipoUsuario = 3;
            trans.Apellido = ".";
            trans.Telefono = int.Parse(txtTelefono.Text);

            bool r = trans.RegistrarTransportista();

            if (r)
            {
                new W_MessageBox().ShowDialog();
                txtRut.Text = string.Empty;
                txtDireccion.Text = string.Empty;
                txtEmpresa.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtTelefono.Text = string.Empty;

            }
            else
            {
                System.Windows.MessageBox.Show("Error inesperado");
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

    }
}
