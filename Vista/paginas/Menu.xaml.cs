﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using System.ComponentModel;
using System.Globalization;

using System.Windows.Markup;


using Vista;

namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {

        bool EstadoCerrado = true;
        public string TokenUser;

        public string Nombre;
        public string Email;
        public string Rut;



        public Menu()
        {
            InitializeComponent();

            var ini = new paginas.Inicio();

            
            frMenu.Content = ini;
            
            
            

        }

        private void btnTransporte_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new paginas.rTransporte());

        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            cerrar();
        }

        public void cerrar()
        {
            if (EstadoCerrado)
            {
                Storyboard sb = this.FindResource("OpenMenu") as Storyboard;

                sb.Begin();


            }
            else
            {
                Storyboard sb = this.FindResource("CloseMenu") as Storyboard;

                sb.Begin();
            }

            EstadoCerrado = !EstadoCerrado;
        }


        //Navegacion
        private void btnSCompra_Click(object sender, RoutedEventArgs e)
        {
            frMenu.Content = SingleTransporte.GetInstance();
        }

        private void btnTransportistas_Click(object sender, RoutedEventArgs e)
        {
            frMenu.Content = new ResgistroTransportista();
        }

        private void btnPerfil_Click(object sender, RoutedEventArgs e)
        {
            Perfil perf = SinglePerfil.GetInstance();

            //perf.tbRut.Text = TokenUser;
            perf.txtNombre.Text = Nombre;
            perf.txtEmail.Text = Email;
            perf.tbRut.Text = Rut;
            frMenu.Content = perf;
            
        }

        private void btnCerrarSesion_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Login());
        }


        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            frMenu.Content = new Reporte();
        }

        //Singleton
        class SingleTransporte : rTransporte
        {
            private SingleTransporte() { }

            private static SingleTransporte _instance;

            public static SingleTransporte GetInstance()
            {
                if (_instance == null)
                {
                    _instance = new SingleTransporte();
                }
                return _instance;
            }
        }

        class SingleTransportista : ResgistroTransportista
        {
            private SingleTransportista() { }

            private static SingleTransportista _instance;

        }

        class SinglePerfil : Perfil
        {
            private SinglePerfil() { }

            private static SinglePerfil _instance;

            public static SinglePerfil GetInstance()
            {
                if (_instance == null)
                {
                    _instance = new SinglePerfil();
                }
                return _instance;
            }
        }

        private void btnProductores_Click(object sender, RoutedEventArgs e)
        {
            frMenu.Content = new Productores();
        }

        private void btnProceso_Click(object sender, RoutedEventArgs e)
        {
            frMenu.Content = new Procesos();
        }
    }
}
