﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para Procesos.xaml
    /// </summary>
    public partial class Procesos : Page
    {
        public Procesos()
        {
            InitializeComponent();


            var ListP = new List<Persona>();


             for (int i = 0; i < 11; i++)
             {
                 var p = new Persona();

                 p.Nombre = i + 1000.ToString();
                 p.Apellido = i + 100.ToString();
                 p.Edad = i;

                 ListP.Add(p);


             }

             tablaProceso.ItemsSource = ListP;
         }
        public class Persona
         {
             public string Nombre { get; set; }
             public string Apellido { get; set; }
             public int Edad { get; set; }
         }


        }

    }

