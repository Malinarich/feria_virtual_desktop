﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controlador.Clases;



namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
            
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
           Usuario user = new Usuario();

            user.Email = txtUsername.Text;
            user.Password = txtContrasenia.Password;

            TokenMessage TM = new TokenMessage();

            TM = user.IniciarSesion();
        

            
            
            
            if (TM.TipoUsuario == "4")//administrador
            {
                paginas.Menu m = new Menu();
                m.TokenUser = TM.Token;
                m.Nombre = TM.Nombre+" "+TM.Apellido;   
                m.Email = TM.Email;
                m.Rut = TM.Rut;


                NavigationService.Navigate(m);
            }
            else if (TM.TipoUsuario == "5")//consultor
            {

                consultor.Menu m = new consultor.Menu();
                
                m.Nombre = TM.Nombre + " " + TM.Apellido;
                m.Email = TM.Email;
                m.Rut = TM.Rut;

                NavigationService.Navigate(m);
            }
            else if (TM.TipoUsuario != "0")
            {
                txtMessage.Text = "Datos incorrectos";
            }
            else
            {
                txtMessage.Text = TM.Message;
            }
        
            //NavigationService.Navigate(new paginas.Menu());
        }
    }
}
