﻿using iText.IO.Font.Constants;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controlador.Clases;
using System.Data.OracleClient;
using System.Diagnostics;
using iText.IO.Image;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;

namespace Vista.paginas
{
    /// <summary>
    /// Lógica de interacción para Reporte.xaml
    /// </summary>
    public partial class Reporte : Page
    {

        public Reporte()
        {
            InitializeComponent();
            dpDesde.DisplayDateEnd = DateTime.Today;
            dpHasta.DisplayDateEnd = DateTime.Today;

            dpDesde.SelectedDate = DateTime.Today;
            dpHasta.SelectedDate = DateTime.Today;
        }

        private void btnReportePDF_Click_1(object sender, RoutedEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\reporte.pdf";
            var pdfWriter = new PdfWriter(path);
            var pdf = new PdfDocument(pdfWriter);
            var documento = new Document(pdf, PageSize.LETTER);

            documento.SetMargins(60, 20, 55, 20);

            var fontColumnas = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
            var fontContenido = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);

            string[] columnas = { "Producto", "Toneladas" };
            float[] tamanos = { 4, 4 };
            var tabla = new Table(UnitValue.CreatePercentArray(tamanos));
            tabla.SetWidth(UnitValue.CreatePercentValue(100));

            foreach (var columna in columnas)
            {
                tabla.AddHeaderCell(new Cell().Add(new Paragraph(columna).SetFont(fontColumnas)));
            }

            var datos = Controlador.Clases.Reporte.GetReporte(dpDesde.SelectedDate.Value, dpHasta.SelectedDate.Value);


            foreach (var d in datos)
            {
                tabla.AddCell(new Cell().Add(new Paragraph(d.Producto).SetFont(fontContenido)));
                tabla.AddCell(new Cell().Add(new Paragraph(d.Toneladas.ToString()).SetFont(fontContenido)));

            }

            string month, year;

            try
            {
                month = dpDesde.SelectedDate.Value.ToString("MMMM");
                year = dpDesde.SelectedDate.Value.Year.ToString();
            }
            catch (Exception)
            {
                month = DateTime.Today.ToString("MMMM");
                year = DateTime.Today.Year.ToString();
            }

            var logo = new iText.Layout.Element.Image(ImageDataFactory.Create("../../paginas/imagenes/feriavirtual4xt.png")).SetWidth(100);

            documento.Add(logo);

            var p = new Paragraph("Reporte de ventas: 7 Productos mas vendidos").SetFont(fontColumnas).SetFontSize(16);
            p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            documento.Add(p);


            var desde = dpDesde.SelectedDate.Value.ToString("dd/MM/yy");
            var hasta = dpHasta.SelectedDate.Value.ToString("dd/MM/yy");

            var pe = new Paragraph(string.Format("({0} - {1})", desde, hasta)).SetFont(fontColumnas).SetFontSize(16);
            pe.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            documento.Add(pe);

            documento.Add(tabla);

            using (var ch = new Chart())
            {
                ch.ChartAreas.Add(new ChartArea());
                ch.Size = new System.Drawing.Size(350, 350);
                var s = new Series();
                s.ChartType = SeriesChartType.Pie;
                s.IsValueShownAsLabel = true;
                foreach (var pnt in datos)
                {
                    s.Points.AddXY(pnt.Producto, pnt.Toneladas);
                    ch.Legends.Add(new Legend(pnt.Producto));
                }

                ch.Series.Add(s);

                ch.SaveImage(@"piechart.png", ChartImageFormat.Png);
            }


            var chart = new iText.Layout.Element.Image(ImageDataFactory.Create("piechart.png"));

            chart.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            documento.Add(chart);

            documento.Close();

            Process.Start(path);
        }

        private void btnReportePDF_Click_2(object sender, RoutedEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\reporte.pdf";
            var pdfWriter = new PdfWriter(path);
            var pdf = new PdfDocument(pdfWriter);
            var documento = new Document(pdf, PageSize.LETTER);

            documento.SetMargins(60, 20, 55, 20);

            var fontColumnas = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
            var fontContenido = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);

            string[] columnas = { "Empresa transportista", "Puntuación promedio" };
            float[] tamanos = { 4, 2 };
            var tabla = new Table(UnitValue.CreatePercentArray(tamanos));
            tabla.SetWidth(UnitValue.CreatePercentValue(100));
            tabla.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);

            foreach (var columna in columnas)
            {
                tabla.AddHeaderCell(new Cell().Add(new Paragraph(columna).SetFont(fontColumnas).SetPaddingLeft(3)));
            }

            var rango = 4;
            if (!string.IsNullOrWhiteSpace(txtRango.Text))
            {
                rango = int.Parse(txtRango.Text);
            }
            var datos = ReporteTransportistas.GetReporte(rango);


            foreach (var d in datos)
            {
                tabla.AddCell(new Cell().Add(new Paragraph($"{d.Transportista.Nombre} {d.Transportista.Apellido}").SetFont(fontContenido).SetPaddingLeft(3)));
                tabla.AddCell(new Cell().Add(new Paragraph(d.Puntuacion.ToString()).SetFont(fontContenido).SetPaddingLeft(3)));

            }

            var logo = new iText.Layout.Element.Image(ImageDataFactory.Create("../../paginas/imagenes/feriavirtual4xt.png")).SetWidth(100);

            documento.Add(logo);

            var p = new Paragraph("Reporte ranking de transportistas").SetFont(fontColumnas).SetFontSize(20);
            p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            documento.Add(p);
            documento.Add(new Paragraph("").SetFont(fontColumnas).SetFontSize(22));

            documento.Add(tabla);
            documento.Add(new Paragraph("\tEstos datos fueron extraidos de la encuesta de satisfacción.").SetFont(fontColumnas).SetFontSize(12));
            documento.Add(new Paragraph("").SetFont(fontColumnas).SetFontSize(18));

            // Grafico de barras
            using (var ch = new Chart())
            {
                ch.ChartAreas.Add(new ChartArea());
                ch.Size = new System.Drawing.Size(700, 350);
                var s = new Series();
                s.ChartType = SeriesChartType.Column;
                s.IsValueShownAsLabel = true;
                s.LabelForeColor = System.Drawing.Color.Black; // Color de las label
                //s.BorderColor = System.Drawing.Color.FromArgb(40, 167, 69);
                //s.MarkerBorderColor = System.Drawing.Color.FromArgb(40, 167, 69);
                //s.BorderColor = System.Drawing.Color.FromArgb(40, 167, 69);
                //ch.BackColor = System.Drawing.Color.FromArgb(40, 167, 69); // Color de fondo
                //ch.ForeColor = System.Drawing.Color.FromArgb(40, 167, 69);
                
                //ch.BorderlineColor = System.Drawing.Color.FromArgb(40, 167, 69);
                //s.LabelBorderColor = System.Drawing.Color.FromArgb(40, 167, 69); // Borde de las label
                s.LabelBackColor = System.Drawing.Color.White; // Fondo de las label
                s.Font = new System.Drawing.Font("Helvetica", 12);
                s.Color = System.Drawing.Color.FromArgb(40, 167, 69); // Color de las columnas
                //s.BorderColor = System.Drawing.Color.Black; // Color borde de las columnas
                //s.BorderWidth = 2; // Ancho bordes de las columnas

                foreach (var pnt in datos)
                {
                    s.Points.AddXY($"{pnt.Transportista.Nombre} {pnt.Transportista.Apellido}", pnt.Puntuacion);
                    //ch.Legends.Add(new Legend($"#{pnt.Transportista.Id} {pnt.Transportista.Nombre} {pnt.Transportista.Apellido}"));
                }

                ch.Series.Add(s);

                ch.SaveImage(@"piechart.png", ChartImageFormat.Png);
            }


            var chart = new iText.Layout.Element.Image(ImageDataFactory.Create("piechart.png"));

            chart.SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER);
            documento.Add(chart);

            documento.Close();

            Process.Start(path);
        }
    }

}

