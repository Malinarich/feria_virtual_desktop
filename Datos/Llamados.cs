﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Llamados
    {
        

        static public string IniciarSesion(string usuarioJSON)
        {
            ServicioWebWCF.ServicioClient cliente = new ServicioWebWCF.ServicioClient();

            //var vJSON = cliente.IniciarSesion("{\"password\":\"admin1\",\"tipouserid\":\"\",\"username\":\"admin1\"}");

            var vJSON = cliente.IniciarSesion(usuarioJSON);
            
            return vJSON;
        }
    }
}
